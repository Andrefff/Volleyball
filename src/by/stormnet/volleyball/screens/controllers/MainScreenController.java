package by.stormnet.volleyball.screens.controllers;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class MainScreenController {

    public Button buttonExit;
    public Button buttonScore;
    public TextField textFieldUserOne;
    public TextField textFieldUserTwo;
    public Button buttonStart;
    public Button buttonSound;

    public void exitClickedBtn(MouseEvent mouseEvent) {
    }

    public void scoreClickedBtn(MouseEvent mouseEvent) {
    }

    public void startClickedBtn(MouseEvent mouseEvent) {
    }

    public void soundClickedBtn(MouseEvent mouseEvent) {
    }
}
