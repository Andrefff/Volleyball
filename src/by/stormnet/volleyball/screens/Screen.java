package by.stormnet.volleyball.screens;

public enum Screen {
    MAIN_SCREEN {
        @Override
        public String getStringValue() {
            return "MainMenuVeiws";
        }
    },
    SCORE_SCREEN {
        @Override
        public String getStringValue() {
            return "ScoreScreenVeiws";
        }
    },
    GAME_SCREEN {
        @Override
        public String getStringValue() {
            return "GameScreenVeiws";
        }
    };
    public abstract String getStringValue();

}
