package by.stormnet.volleyball.screens;

import javafx.scene.Scene;
import javafx.stage.Stage;

import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

public class ScreenManager {

    private final double SCREEN_WIDTH = 1024;
    private final double SCREEN_HEIGHT=600;
    private Stage window;
    private Map<Screen, Scene> screens = null;
    private static volatile ScreenManager instance = null;

    private ScreenManager(){
        this.screens = new HashMap<>();
    }

    public static ScreenManager getInstance(){
        if(instance==null){
            synchronized (ScreenManager.class){
                if(instance==null){
                    instance=new ScreenManager();
                }
            }

        }
        return instance;

    }

    public Stage getWindow(){
        return window;
    }

    public void setWindow(Stage window){
        this.window = window;
        this.window.setHeight(SCREEN_HEIGHT);
        this.window.setWidth(SCREEN_WIDTH);
        this.window.setResizable(false);

    }

    private Screen createScene(Screen screen){


        return null;
    }

}
